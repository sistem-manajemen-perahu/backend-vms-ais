<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\SendLocation;
use App\Models\Position;
use App\Models\Tracking;
use Illuminate\Http\Request;

class GenerateRandomLatLng extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:randomlatlng';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    function random_float($min, $max)
    {

        return ($min + lcg_value() * (abs($max - $min)));
    }
    public function handle()
    {
        $latestPosition = Position::orderBy("created_at", "DESC")->first();
        // dd($latestTrackin);
        if ($latestPosition !== null) {

            $randomLat = round($this->random_float(0.0001, 0.0006), 4);
            $randomLong = round($this->random_float(0.001, 0.006), 4);
            $parseLat = (float)$latestPosition->latitude;
            // dd($parseLat);
            $parseLng = (float)$latestPosition->longitude;
            $lat = round($parseLat - $randomLat, 4);
            $lng = round($parseLng + $randomLong, 4);
            // dd($lat);
            $position = Position::create([
                "latitude" => $lat,
                "longitude" => $lng,
            ]);
            // dd($tracking);
            // dd($parseLat);
            SendLocation::dispatch($lng, $lat);
        } else {
            $lat = -6.24;
            $lng = 108.32;
            Position::create([
                "latitude" => $lat,
                "longitude" => $lng,
            ]);
            SendLocation::dispatch($lng, $lat);
        }
        // $random = round($this->random_float(0.0001, 0.0002), 4);
        // dd($random);

        // dd([
        //     $lat,
        //     $lng
        // ]);
        print("command sedang berjalan");
        return [];
    }
}
