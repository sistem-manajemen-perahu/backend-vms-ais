<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fisherman extends Model
{
    use HasFactory;

    protected $fillable = ['vessel_id', 'name', 'gender', 'address', 'phone_number'];


    public function vessel()
    {
        return $this->belongsTo(Vessel::class);
    }
}
