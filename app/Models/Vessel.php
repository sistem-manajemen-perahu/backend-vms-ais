<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vessel extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'imo', 'mmsi', 'call_sign', 'flag', 'type_generic', 'type_detailed', 'status', 'gross_tonnage', 'summer_dwt', 'year_built'];

    public function position()
    {
        return $this->hasMany(Position::class);
    }
}
