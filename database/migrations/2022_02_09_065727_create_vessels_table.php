<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vessels', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('imo');
            $table->string('mmsi');
            $table->string('call_sign');
            $table->string('flag');
            $table->string('type_generic');
            $table->string('type_detailed');
            $table->string('status');
            $table->integer('gross_tonnage');
            $table->string('summer_dwt');
            $table->string('length_overall');
            $table->string('breadth_extream');
            $table->string('year_built');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vessels');
    }
};
