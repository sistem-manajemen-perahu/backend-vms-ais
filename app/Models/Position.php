<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Position extends Model
{

    protected $fillable = ['vessel_id', 'speed', 'course', 'ais_source', 'latitude', 'longitude', 'status'];
    // protected $table = 'Position';
    use HasFactory;

    public function vessel()
    {
        return $this->belongsTo(Vessel::class);
    }
}
