<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Fisherman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

use function PHPUnit\Framework\isNull;

class FishermanController extends Controller
{
    /**
     * Display a listing of the resource.
     * @group
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fishermen = Fisherman::all();
        if (count($fishermen) > 0) {
            return response()->json([
                'success' => true,
                'message' => 'Get All Data Fisherman',
                'data' => $fishermen
            ], Response::HTTP_OK);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Data not found',
                'data' => null
            ], Response::HTTP_NOT_FOUND);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @bodyParam vessel_id int The id of the vessel. Example : 2
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'phone_number' => 'required',
        ], [
            'name.required' => 'Field nama tidak boleh kosong',
            'gender.required' => 'Field jenis kelamin tidak boleh kosong',
            'address.required' => 'Field alamat tidak boleh kosong',
            'phone_number.required' => 'Field nomor telepon tidak boleh kosong',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        $input = $request->onlyl(['vessel_id', 'name', 'gender', 'address', 'phone_number']);
        $fisherman = Fisherman::create([
            'vessel_id' => $input['vessel_id'],
            'name' => $input['name'],
            'gender' => $input['gender'],
            'address' => $input['address'],
            'phone_number' => $input['phone_number'],
        ]);

        if ($fisherman) {
            return response()->json([
                'success' => true,
                'message' => 'Success storing data fisherman',
                'data' => $fisherman
            ], Response::HTTP_CREATED);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Failed storing data fisherman',
                'data' => null
            ], Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fisherman = Fisherman::find($id);

        // dd($fisherman);
        return $fisherman !== null  ? response()->json([
            'success' => true,
            'message' => 'Display specified data',
            'data' => $fisherman,
        ], Response::HTTP_OK) : response()->json([
            'success' => false,
            'message' => 'Data not found',
            'data' => null,
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fisherman = Fisherman::find($id);
        $fisherman->update($request->onlyl(['vessel_id', 'name', 'gender', 'address', 'phone_number']));
        $validator = Validator::make([
            'phone_number' => 'unique:fisherman,phone_number,except, ' . $id,
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_BAD_REQUEST);
        }
        return $fisherman  ? response()->json([
            'success' => true,
            'message' => 'Successfully updating data fisherman',
            'data' => $fisherman,
        ], Response::HTTP_OK) : response()->json([
            'success' => false,
            'message' => 'Data not found',
            'data' => null,
        ], Response::HTTP_NOT_FOUND);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
