<?php

namespace Database\Seeders;

use App\Models\Position;
use App\Models\Vessel;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VesselDummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $vessels = [
            [
                'name' => 'ASEAN RAIDER II',
                'imo' => '9149639',
                'mmsi' => '56331000',
                'call_sign' => '',
                'flag' => 'Singapore[SG]',
                'type_generic' => 'Passenger',
                'type_detailed' => 'Passenger Ship',
                'status' => 'Active',
                'gross_tonnage' => '141',
                'summer_dwt' => '21 t',
                'year_built' => '1997',
                'length_overall' => '30.5',
                'breadth_extream' => '6.24',
            ],
            // [
            //     'name' =>,
            //     'imo' => ,
            //     'mmsi' =>,
            //     'call_sign',
            //     'flag' =>,
            //     'type_generic' =>,
            //     'type_detailed'=>,
            //     'status'=>,
            //     'gross_tonnage'=>,
            //     'summer_dwt'=>,
            //     'year_built'=>,
            //     'length_overall'=>30.5,
            //     'breadth_extream'=>6.24,
            // ],
        ];
        $insertVessel = Vessel::create($vessels[0]);

        $positions = [
            [
                'vessel_id' => $insertVessel->id,
                'speed' => '8.6',
                'course' => '212',
                'ais_source' => 'Terestrial AIS',
                'latitude' => '1.137767',
                'longitude' => '104.0588',
                'status' => 'Underway using engine',
            ],
        ];
        Position::create($positions[0]);
    }
}
